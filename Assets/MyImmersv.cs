﻿using System;
using UnityEngine;
using Immersv;

using System.Collections;

public class MyImmersv : MonoBehaviour
{
    float timeToWait = 0;
    SDKExample immersv;
    void Start()
    {
        Debug.Log("Starting SDKImmersv");
        immersv = new SDKExample();
        immersv.PrepareSDK();
    }

    void Update()
    {
        
        if (( Time.timeSinceLevelLoad > timeToWait+5))
        {
            timeToWait = Time.timeSinceLevelLoad;

            Debug.Log("calling ads");
            ShowAd_Immersv();
        }
            
    }

    public void ShowAd_Immersv()
    {
        immersv.ShowAd();
    }
}

    //This is only a minimal example and will likely need to be altered to fit your use case.
    public class SDKExample //You can get the application and placement ID's from the dashboard
{
    // http://dashboard.immersv.io/
    private const string APPLICATION_ID = "7303-4932-0090";
    private const string PLACEMENT_ID = "5923-5907-0094";
    private bool _isSDKReady = false;
    public void PrepareSDK() //Call this method once when your game starts. The SDK will stay
    { // Initialized for the duration of your apps android lifecycle
        ImmersvSDK.OnInitSuccess += () =>
        {
            _isSDKReady = true;
            Debug.Log("Immersv ready");
            ImmersvSDK.Ads.OnAdReady += () => // When the call to LoadAd below is finished,
            { // this callback will start the ad view immediately
                ImmersvSDK.Ads.StartAd();
                Debug.Log("Immersv ads");
            };
            ImmersvSDK.Ads.OnAdComplete += (AdViewResult result) =>
            {
                if (result.BillableCriteriaMet)
                { //If you are incentivizing the user to the view the ad, you can reward
                  //Them here
                  //RewardUser();
                    Debug.Log("Immersv reward");
                }
            };
        };
        ImmersvSDK.Init(APPLICATION_ID);
    }

    public void ShowAd() //Call this method once when you want to show an ad. You can and should decouple the loading and showing of an ad for better user expereince.
    {
        if (!_isSDKReady) //This will be set to true by the callback above once the SDK isready
        {
            Debug.Log("Immersv ads not ready!");

            return;
        }
        Debug.Log("Ads.LoadAd (PLACEMENT_ID)");
        ImmersvSDK.Ads.LoadAd (PLACEMENT_ID);
    }
}
